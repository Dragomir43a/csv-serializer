#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX_LINE_SIZE 1024

void serialize_csv(FILE* csv_file, FILE* bin_file);
void deserialize_csv(FILE* bin_file, FILE* csv_file);

bool logs = true;
int main(int argc, char* argv[]) {
    //проверка за логове
    for (int i = 1; i < argc; i++){
        if(strcmp(argv[i], "-s") == 0){
            logs = false;
        }
    }
    // Проверка за аргументи
    if (argc < 4) {
        if(logs){
            printf("Not enough arguments. Usage: %s -f [csv] -c [serialize/deserialize]\n", argv[0]);
        }
        return 1;
    }
    // Определяне на пътя до файловете
    char* csv_path = NULL;
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-f") == 0) {
            i++;
            csv_path = argv[i];
        }
        else if (strcmp(argv[i], "-c") == 0) {
            i++;
            if (strcmp(argv[i], "serialize") == 0) {
                // Сериализиране
                char bin_path [50] = "serialized.bin";
                FILE* csv_file = fopen(csv_path, "r");
                FILE* bin_file = fopen(bin_path, "wb");
                if (csv_file && bin_file) {
                    serialize_csv(csv_file, bin_file);
                    if(logs){
                        printf("Successfully serialized to file %s\n", bin_path);
                    }
                }
                else if(logs){
                    printf("Error open files\n");
                }
                fclose(csv_file);
                fclose(bin_file);
            }
            else if (strcmp(argv[i], "deserialize") == 0) {
                // Десериализиране
                FILE* csv_file = fopen("deserialized.csv", "w");
                FILE* bin_file = fopen(csv_path, "rb");
                if (csv_file && bin_file) {
                    deserialize_csv(bin_file, csv_file);
                    if(logs){
                        printf("Successfully deserialized to file deserialized.csv\n");
                    }
                }
                else if(logs){
                    printf("Error open files\n");
                }
                fclose(csv_file);
                fclose(bin_file);
            }
            else if(logs){
                printf("Invalid command: %s -f [път_до_csv_файл] -c [serialize/deserialize]\n", argv[0]);
                return 1;
            }
        }
    }
    return 0;
}

void serialize_csv(FILE* csv_file, FILE* bin_file) {
    char line[MAX_LINE_SIZE];
    while (fgets(line, MAX_LINE_SIZE, csv_file)) {
        // Пропускане на празни редове и коментари
        if (line[0] == '\n' || line[0] == '#') {
            continue;
        }
        // Парсване на данните от CSV реда
        char* token = strtok(line, ",");
        while (token != NULL) {
            // Записване на данните в бинарния файл
            fwrite(token, strlen(token), 1, bin_file);
            fwrite(",", 1, 1, bin_file);
            token = strtok(NULL, ",");
        }
        // Записване на край на реда в бинарния файл
        fwrite("\n", 1, 1, bin_file);
    }
}

void deserialize_csv(FILE* bin_file, FILE* csv_file) {
    char line[MAX_LINE_SIZE];
    while (fread(line, MAX_LINE_SIZE, 1, bin_file)) {
        // Намиране на края на реда
        char* newline_pos = strchr(line, '\n');
        if (newline_pos == NULL) {
            continue; // Редът е по-дълъг от максимално допустимата дължина
        }
        // Заместване на края на реда със запетая
        *newline_pos = ',';
        // Парсване на данните от бинарния ред
        char* token = strtok(line, ",");
        while (token != NULL) {
            // Записване на данните в CSV файл
            fprintf(csv_file, "%s", token);
            token = strtok(NULL, ",");
            if (token != NULL) {
                fprintf(csv_file, ",");
            }
        }
        // Записване на край на реда в CSV файл
        fprintf(csv_file, "\n");
    }
}

